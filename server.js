let http = require('http');
let fs = require('fs');
let path = require('path');
const { randomUUID } = require('crypto');

function read(file)
{
	return fs.readFileSync(path.join(__dirname, file));
}

function save()
{
	fs.writeFileSync(path.join(__dirname, "data.json"), JSON.stringify(data));
}

let data = JSON.parse(read("data.json"))

let server = http.createServer((request, response) =>
{
	if (request.url === "/")
	{
		response.end(read("pages/index.html"))
	}
	else if (request.url === "/content")
	{
		let role = request.headers["role"];
		let page = role == "admin" ? "pages/admin.html" : "pages/non-admin.html";
		response.end(read(page));
	}
	else if (request.url === "/scripts")
	{
		let role = request.headers["role"];
		let page = role == "admin" ? "scripts/admin.js" : "scripts/non-admin.js";
		response.end(read(page));
	}
	else if (request.url.startsWith("/cards"))
	{
		if (request.method === "GET")
		{
			response.end(JSON.stringify(data));
		}
		else if (request.method === "POST")
		{
			let id = randomUUID();
			let text = request.headers["text"];
			let newCard = {id, text};
			data.push(newCard);
			save();
			response.end(JSON.stringify(newCard));
		}
		else if (request.method === "DELETE")
		{
			let id = request.url.replace("/cards/", "");
			data = data.filter(x => x.id != id);
			save();
			response.end();
		}
	}
});

server.listen(3000);
