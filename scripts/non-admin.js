fetch("/cards").then(x => x.json()).then(x =>
   {
      for (card of x)
      {
         renderCard(card);
      }
   });

   create.onclick = () =>
   {
      fetch("/cards", {method: "POST", headers: {text: newCard.value}}).then(x => x.json()).then(x =>
      {
         renderCard(x);
      });
   }

   function renderCard(card)
   {
      let button = document.createElement("button");
      button.textContent = `${card.id}: ${card.text}`;
      document.body.append(button);
      button.onclick = () =>
      {
         fetch(`/cards/${card.id}`, {method: "DELETE"}).then(x =>
         {
            button.remove();
         });
      }
   }